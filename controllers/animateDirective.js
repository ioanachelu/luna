(function () {

    var mainApp = angular.module('mainApp');

    mainApp.directive('animate', function () {
        var linker = function (scope, element, attrs) {
            scope.up = function () {
                $(this).animate({
                    top: '-=150'
                })
            }

            scope.down = function () {
                $(this).animate({
                    top: '+=150'
                })
            }

            scope.right = function () {
                $(this).animate({
                    left: '-=150'
                })
            }

            scope.left = function () {
                $(this).animate({
                    left: '+=150'
                })
            }

            var direction = attrs['animate'];

            element.on('click', scope[direction]);
        };

        return {
            restrict: 'A',
            link: linker
        }
    });


}());