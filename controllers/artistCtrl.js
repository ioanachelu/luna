(function () {

    var mainApp = angular.module('mainApp');

    mainApp.controller('ArtistCtrl', function ($scope, $routeParams) {
        var artistSlides = [['../../img/home/artists/Adda/6.jpg','../../img/home/artists/Adda/6.jpg','../../img/home/artists/Adda/2.jpg','../../img/home/artists/Adda/5.jpg','../../img/home/artists/Adda/8.jpg','../../img/home/artists/Adda/8.jpg'],[]];
        var artistSocials = [["https://www.facebook.com/addaofficialmusic"], ["https://www.facebook.com/angelo.simonica"]];
        var artistNames = ["ADDA","Angelo Simonica"];
        var artistDescriptions = ["LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. AENEAN LACUS NULLA, MATTIS SED SCELERISQUE AC, ORNARE VEL NEQUE. NUNC COMMODO SIT AMET VELIT NON IMPERDIET. PELLENTESQUE ULLAMCORPER ANTE NISL. PRAESENT ELEIFEND POSUERE LOREM ET TRISTIQUE. SED VEL LIBERO VITAE LACUS MALESUADA SCELERISQUE. DUIS DIGNISSIM FACILISIS CURSUS. ETIAM NISL ODIO, ACCUMSAN SUSCIPIT NULLA SIT AMET, FACILISIS IMPERDIET NIBH. MAURIS LACINIA, LECTUS VITAE EUISMOD DIGNISSIM, ENIM LOREM CONSECTETUR RISUS, VITAE RHONCUS LIBERO ORCI VITAE EST. MORBI MAXIMUS VELIT UT NISL PELLENTESQUE SODALES. MAURIS AT URNA MATTIS, MATTIS EST SED, TINCIDUNT METUS. PROIN VOLUTPAT ODIO AT NEQUE VIVERRA ALIQUAM. UT RUTRUM TEMPOR IACULIS. PROIN IN ULTRICIES PURUS, QUIS CONSEQUAT ORCI. FUSCE LUCTUS VELIT IN BIBENDUM LAOREET. ALIQUAM GRAVIDA ODIO DIGNISSIM LOREM FERMENTUM VARIUS.",
                                  "LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. AENEAN LACUS NULLA, MATTIS SED SCELERISQUE AC, ORNARE VEL NEQUE. NUNC COMMODO SIT AMET VELIT NON IMPERDIET. PELLENTESQUE ULLAMCORPER ANTE NISL. PRAESENT ELEIFEND POSUERE LOREM ET TRISTIQUE. SED VEL LIBERO VITAE LACUS MALESUADA SCELERISQUE. DUIS DIGNISSIM FACILISIS CURSUS. ETIAM NISL ODIO, ACCUMSAN SUSCIPIT NULLA SIT AMET, FACILISIS IMPERDIET NIBH. MAURIS LACINIA, LECTUS VITAE EUISMOD DIGNISSIM, ENIM LOREM CONSECTETUR RISUS, VITAE RHONCUS LIBERO ORCI VITAE EST. MORBI MAXIMUS VELIT UT NISL PELLENTESQUE SODALES. MAURIS AT URNA MATTIS, MATTIS EST SED, TINCIDUNT METUS. PROIN VOLUTPAT ODIO AT NEQUE VIVERRA ALIQUAM. UT RUTRUM TEMPOR IACULIS. PROIN IN ULTRICIES PURUS, QUIS CONSEQUAT ORCI. FUSCE LUCTUS VELIT IN BIBENDUM LAOREET. ALIQUAM GRAVIDA ODIO DIGNISSIM LOREM FERMENTUM VARIUS."];
        var artistsSlider = [];
        $scope.artistName = artistNames[$routeParams.artistId];
        $scope.artistId = $routeParams.artistId;
        $scope.artistBio = artistDescriptions[$routeParams.artistId];
        $scope.facebook = artistSocials[$routeParams.artistId][0];
        $scope.imageSlides = artistSlides[$routeParams.artistId];
    }).directive('customSlider', function() {
        $(function() {

    //settings for slider
    var width = $("html").width();

    var animationSpeed = 1000;
    var pause = 3000;
    var currentSlide = 1;

    //cache DOM elements
    var $slider = $('#slider');
    var $slideContainer = $('.slides', $slider);
    var $slides = $('.slide', $slider);
            console.log("length : " + $slides.length);
    var $navArrows = $('#nav-arrows');
    var $dots = $('.dots');
    var $artist = $('.artist');
    var $artistName = $('.artistName');
    var interval;

    function getScrollBarWidth() {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild(inner);
        document.body.appendChild(outer);
        var w1 = inner.offsetWidth;
        outer.style.overflow = 'scroll';
        var w2 = inner.offsetWidth;
        if (w1 == w2) w2 = outer.clientWidth;
        document.body.removeChild(outer);
        return (w1 - w2);
    };

//    width -= getScrollBarWidth();



    $navArrows.children(':last').on('click', function() {
        hideElements();
        nextSlider();
        displayElements();
        return false;
    });

    $navArrows.children(':first').on('click', function() {
        hideElements();
        previousSlider();
        displayElements();
        return false;

    });

    function hideElements() {
        $dots
            .queue(function(next) {
                $(this).fadeOut("fast");
                next();
            });
        $artist
            .queue(function(next) {
                $(this).fadeOut("fast");
                next();
            });
        $artistName
            .queue(function(next) {
                $(this).fadeOut("fast");
                next();
            });
    }

    function displayElements(){
        $dots
            .delay(600)
            .queue(function(next) {
                $(this).fadeIn("fast");
                next();
            });
        $artist
            .delay(800)
            .queue(function(next) {
                $(this).fadeIn("fast");
                next();
            });
        $artistName
            .delay(1000)
            .queue(function(next) {
                $(this).fadeIn("fast");
                next();
            });
    }
    function nextSlider() {
        $slideContainer.delay(600).animate({
            'margin-left': '-=' + width
        }, animationSpeed, function() {
            if (++currentSlide === $slides.length - 1) {
                currentSlide = 1;
                $slideContainer.css('margin-left', -width);
            }
        });
    }

    function previousSlider() {
        $slideContainer.animate({
            'margin-left': '+=' + width
        }, animationSpeed, function() {
            if (--currentSlide === 0) {
                currentSlide = $slides.length-2;
                $slideContainer.css('margin-left', -(width*($slides.length-2)));
            }
        });
    }
    
    function initSlider(){
           $slideContainer.css('margin-left',-width);
    }
    
    initSlider();
    
    //    function startSlider() {
    //        interval = setInterval(function() {
    //            $slideContainer.animate({
    //                'margin-left': '-=' + width
    //            }, animationSpeed, function() {
    //                if (++currentSlide === $slides.length - 1) {
    //                    currentSlide = 1;
    //                    $slideContainer.css('margin-left', 0);
    //                }
    //            });
    //        }, pause);
    //    }
    //
    //    function pauseSlider() {
    //        clearInterval(interval);
    //    }

    //    $slideContainer
    //        .on('mouseenter', pauseSlider)
    //        .on('mouseleave', startSlider);

    //startSlider();


});
      return {
          restrict: 'E',
          transclude: true,
          templateUrl: 'partials/directives/artistSlider.html'
      }
    });
    
    
}());