(function () {

    var mainApp = angular.module('mainApp');

    mainApp.controller('HomeCtrl', function ($scope) {

        $scope.artists = [
            {
                first: {
                    id: 0,
                    link: '#/artist/0',
                    title: 'Adda',
                    description: 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut le',
                    img: "../img/home/artists/Adda/5.jpg"
                },
                second: {
                    id: 1,
                    link: '#/artist/1',
                    title: 'Liviu Teodorescu',
                    description: 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut le',
                    img: "../img/home/artists/LiviuTeodorescu/2.jpg"
                }
            },
            {
                first: {
                    id: 2,
                    link: '#/artist/2',
                    title: 'Ruby',
                    description: 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut le',
                    img: "../img/home/artists/Ruby/ruby.jpg"
                },
                second: {
                    id: 3,
                    link: '#/artist/3',
                    title: 'Angelo Simonica',
                    description: 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut le',
                    img: "../img/home/artists/AngeloSimonica/1.jpg"
                }
            },
            {
                first: {
                    id: 4,
                    link: '#/artist/4',
                    title: 'Amna',
                    description: 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut le',
                    img: "../img/home/artists/Amna/8.jpg"
                },
                second: {
                    id: 5,
                    link: '#/artist/5',
                    title: 'Robert Tome',
                    description: 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut le',
                    img: "../img/home/artists/RobertToma/5.jpg"
                }
            },
            {
                first: {
                    id: 6,
                    link: '#/artist/6',
                    title: 'Shift',
                    description: 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut le',
                    img: "../img/home/artists/Shift/5.jpg"
                },
                second: {
                    id: 7,
                    link: '#/artist/7',
                    title: 'WhatsUp',
                    description: 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut le',
                    img: "../img/home/artists/WhatsUp/4.jpg"
                }
            }
        ];

    });

    mainApp.directive('myBackgroundImage', function () {
     return function (scope, element, attrs) {
         element.css({
             'background-image': 'url(' + attrs.myBackgroundImage + ')',
             'background-size': 'cover',
             'background-repeat': 'no-repeat',
             'background-position': 'center center'
         });
     };
 });
}());