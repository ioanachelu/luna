(function () {

    var mainApp = angular.module('mainApp', ['ui.bootstrap', 'ngRoute', 'ngAnimate']);

    mainApp.config(function ($routeProvider) {
        mainApp.resolveScriptDeps = function (dependencies) {
            return function ($q, $rootScope) {
                var deferred = $q.defer();
                $script(dependencies, function () {
                    // all dependencies have now been loaded by $script.js so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        };
        $routeProvider
            .when("/home", {
                templateUrl: "partials/home.html",
                controller: "HomeCtrl"
            })
            .when("/studios", {
                templateUrl: "partials/pages/studios.html",
                controller: "StudiosCtrl",
                resolve: {
                    deps: mainApp.resolveScriptDeps(['js/home/jquery.ba-cond.min.js',
                                                                    'js/home/jquery.slitslider.js',
                                                                    'js/studios/slider.js'])
                }
            })
            /*.when("/timeline", {
    templateUrl: "partials/pages/timeline.html",
    controller: "TimelineCtrl"
})*/
            .when("/aboutUs", {
                templateUrl: "partials/pages/aboutUs.html",
                controller: "AboutUsCtrl"
            })
            .when("/artist/:artistId", {
                templateUrl: "partials/pages/artist.html",
                controller: "ArtistCtrl"
            })
            .otherwise({
                redirectTo: "/home"
            });
    });

    mainApp.directive('helloMaps', function () {
        return function (scope, elem, attrs) {
            var mapOptions,
                latitude = attrs.latitude,
                longitude = attrs.longitude,
                map;

            latitude = latitude && parseFloat(latitude, 10) || 44.417184;
            longitude = longitude && parseFloat(longitude, 10) || 26.119362;
            var location = new google.maps.LatLng(latitude, longitude);
            var pinSize = 730 / 2;
            var currentPinImage = {
                url: 'img/contact/MAPPOINTER.svg',
                scaledSize: new google.maps.Size(pinSize, pinSize),
                anchor: new google.maps.Point(pinSize / 2 + 1, pinSize / 2 + 30),
            };

            var styles = [{
                "featureType": "landscape",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 65
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 51
                }, {
                    "visibility": "simplified"
                }]
            }, {
                "featureType": "road.highway",
                "stylers": [{
                    "saturation": -100
                }, {
                    "visibility": "simplified"
                }]
            }, {
                "featureType": "road.arterial",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 30
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "road.local",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 40
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "transit",
                "stylers": [{
                    "saturation": -100
                }, {
                    "visibility": "simplified"
                }]
            }, {
                "featureType": "administrative.province",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "lightness": -25
                }, {
                    "saturation": -100
                }]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "hue": "#ffff00"
                }, {
                    "lightness": -25
                }, {
                    "saturation": -97
                }]
            }]

            var styledMap = new google.maps.StyledMapType(styles, {
                name: "Styled Map"
            });
            mapOptions = {
                center: location,
                zoom: 14,
                scrollwheel: false,
                disableDefaultUI: true,
                draggable: false
            };
            map = new google.maps.Map(elem[0], mapOptions);
            var currentPin = new google.maps.Marker({
                position: location,
                map: map,
                icon: currentPinImage
            });

            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');


            // Keep the map responsive
            var center;

            function calculateCenter() {
                center = map.getCenter();
            }
            google.maps.event.addDomListener(map, 'idle', function () {
                calculateCenter();
            });
            google.maps.event.addDomListener(window, 'resize', function () {
                map.setCenter(center);
            });
        }
    });
}());
