(function () {
    "use strict";
    var mainApp = angular.module('mainApp');

    mainApp.constant('TweenMax', TweenMax);

    mainApp.controller('AboutUsCtrl', function ($scope, TweenMax) {
        $scope.pages = [
            {
                id: 0,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Magic',
                year: '2000',
                color: 'pink'
            },
            {
                id: 1,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Sound',
                year: '2001',
                color: 'blue'
            },
            {
                id: 2,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Vision',
                year: '2002',
                color: 'green'
            }
        ];

        $scope.currentPage = 0;
        var nrPages = 2;

        $scope.page = $scope.pages[0];
        $scope.checked = true;

        $scope.isInTransit = false;

        $scope.next = function () {
            var nextPage;
            if ($scope.currentPage == nrPages) {
                nextPage = 0;
            } else {
                nextPage = $scope.currentPage + 1;
            }
            $scope.switchPage(nextPage);
        };

        $scope.previous = function () {
            var nextPage;
            if ($scope.currentPage == 0) {
                nextPage = nrPages;
            } else {
                nextPage = $scope.currentPage - 1;
            }
            $scope.switchPage(nextPage);
        };

        $scope.$on('bgTransitionComplete', function () {
            $scope.isInTransit = false;
        });

        $scope.isCurrentPage = function (page) {
            return $scope.currentPage === page;
        };

        $scope.switchPage = function (page) {
            var i, k, j;

            if (page != 0) {
                $scope.checked = false;
            } else {
                $scope.checked = true;
            }


            if ($scope.currentPage !== page) {
                $scope.page = $scope.pages[page];
                $scope.currentPage = page;
                $scope.isInTransit = true;
            }
        };

    });


    mainApp.directive('bg', function ($window) {
        return function (scope, element, attrs) {

            var resizeBG = function () {
                var bgwidth = 1; //element.width();
                var bgheight = 1; //element.height();

                var winwidth = $window.innerWidth;
                var winheight = $window.innerHeight;

                var widthratio = winwidth / bgwidth;
                var heightratio = winheight / bgheight;

                var widthdiff = heightratio * bgwidth;
                var heightdiff = widthratio * bgheight;

                if (heightdiff > winheight) {
                    element.css({
                        width: winwidth + 'px',
                        height: $window.innerHeight + 'px'
                    });
                } else {
                    element.css({
                        width: widthdiff + 'px',
                        height: $window.innerHeight + 'px'
                    });
                }
            };

            var windowElement = angular.element($window);
            windowElement.resize(resizeBG);

            scope.$watch(attrs.pageId, function (value) {
                console.log(value);
                resizeBG();
            });
            /*element.bind('load', function () {
    resizeBG();
});*/
        }
    });

    mainApp.directive('semibg', function ($window) {
        return function (scope, element, attrs) {
            var resizeBG = function () {
                var bgwidth = 1;
                /*element.width();*/
                var bgheight = 1;
                /*element.height();*/

                var winwidth = $window.innerWidth;
                var winheight = $window.innerHeight;

                var widthratio = winwidth / bgwidth;
                var heightratio = winheight / bgheight;

                var widthdiff = heightratio * bgwidth;
                var heightdiff = widthratio * bgheight;

                if (heightdiff > winheight) {
                    element.css({
                        width: (winwidth / 2) + 'px',
                        height: $window.innerHeight + 'px'
                    });
                } else {
                    element.css({
                        width: (widthdiff / 2) + 'px',
                        height: $window.innerHeight + 'px'
                    });
                }
            };

            var windowElement = angular.element($window);
            windowElement.resize(resizeBG);

            scope.$watch(attrs.pageId, function (value) {
                console.log(value);
                resizeBG();
            });
        }
    });

    mainApp.animation('.bg-animation', function ($window, $rootScope, TweenMax) {
        return {
            enter: function (element, done) {
                TweenMax.fromTo(element, 0.5, {
                    top: -$window.innerHeight
                }, {
                    top: 0,
                    onComplete: function () {
                        $rootScope.$apply(function () {
                            $rootScope.$broadcast('bgTransitionComplete');
                        });
                        done();
                    }
                });
            },

            leave: function (element, done) {
                TweenMax.to(element, 0.5, {
                    top: $window.innerHeight,
                    onComplete: done
                });
            }
        };
    });

    /*
        mainApp.animation('.panel-animation-left', function (TweenMax) {
            return {
                addClass: function (element, className, done) {
                    if (className == 'ng-hide') {
                        TweenMax.to(element, 0.2, {
                            opacity: 0,
                            onComplete: done
                        });
                    } else {
                        done();
                    }
                },
                removeClass: function (element, className, done) {
                    if (className == 'ng-hide') {
                        element.removeClass('ng-hide');
                        TweenMax.fromTo(element, 0.5, {
                            opacity: 0,
                            left: -element.width()
                        }, {
                            opacity: 0.8,
                            left: 0,
                            onComplete: done
                        });
                    } else {
                        done();
                    }
                }
            };
        });
    */

    /*    mainApp.animation('.panel-animation-right', function ($window, TweenMax) {
            return {
                addClass: function (element, className, done) {
                    if (className == 'ng-hide') {
                        TweenMax.to(element, 0.2, {
                            opacity: 0,
                            onComplete: done
                        });
                    } else {
                        done();
                    }
                },
                removeClass: function (element, className, done) {
                    if (className == 'ng-hide') {
                        element.removeClass('ng-hide');
                        TweenMax.fromTo(element, 0.5, {
                            opacity: 0,
                            right: -element.width()
                        }, {
                            opacity: 0.8,
                            right: 0 ,
                            onComplete: done
                        });
                    } else {
                        done();
                    }
                }
            };
        });*/

    mainApp.animation('.panel-animation-right', function ($window, TweenMax) {
        return {
            enter: function (element, done) {
                TweenMax.fromTo(element, 1, {
                    opacity: 0,
                    right: -element.width()
                }, {
                    delay: 1,
                    opacity: 0.8,
                    right: 0 /*($window.innerHeight / 2)*/ ,
                    onComplete: done

                });
            },
            leave: function (element, done) {
                TweenMax.to(element, 0.2, {
                    opacity: 0,
                    onComplete: done
                });
            }
        };
    });



    mainApp.animation('.text-animation-rotate', function ($window, TweenMax) {
        return {
            leave: function (element, done) {

                TweenMax.set(element, {
                    x: "-50%",
                    y: "-50%"
                });
                TweenMax.fromTo(element, 1, {
                    rotation: -90,
                    transformOrigin: "bottom 50%",
                    x: -($window.innerWidth / 4)
                }, {
                    rotation: 0,
                    transformOrigin: "bottom 50%",
                    x: 0,
                    onComplete: done
                });

            },
            enter: function (element, done) {
                TweenMax.set(element, {
                    x: "-50%",
                    y: "-50%"
                });
                TweenMax.fromTo(element, 1, {

                    rotation: 0

                }, {
                    delay: 1,
                    rotation: -90,
                    transformOrigin: "bottom 50%",
                    x: -($window.innerWidth / 4),
                    onComplete: done
                });

            }
        };
    });

    mainApp.directive('bgColor', function () {

        return function (scope, element, attrs) { //link function 

            scope.$watch(attrs.currentColor, function (value) {

                element.css('background-color', value);

            });

        }
    });
}());