(function () {

    var mainApp = angular.module('mainApp');

    mainApp.constant('TweenMax', TweenMax);

    mainApp.controller('TimelineCtrl', function ($scope) {
        $scope.currentPage = 0;
        var nrBreakpoints = 8;

        $scope.pages = [
            {
                id: 0,
                img: '../../img/timeline/blur/0-blur.jpg',
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Magic',
                sublabel: '2000',
                photo: '../../img/timeline/0.jpg'
            },
            {
                id: 1,
                img: "../../img/timeline/blur/1-blur.jpg",
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Sound',
                sublabel: '2001',
                photo: '../../img/timeline/1.jpg'
            },
            {
                id: 2,
                img: "../../img/timeline/blur/2-blur.jpg",
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Vision',
                sublabel: '2002',
                photo: '../../img/timeline/2.jpg'
            },
            {
                id: 3,
                img: "../../img/timeline/blur/3-blur.jpg",
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Art',
                sublabel: '2003',
                photo: '../../img/timeline/3.jpg'
            },
            {
                id: 4,
                img: "../../img/timeline/blur/4-blur.jpg",
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Imagination',
                sublabel: '2004',
                photo: '../../img/timeline/4.jpg'
            },
            {
                id: 5,
                img: "../../img/timeline/blur/5-blur.jpg",
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Luck',
                sublabel: '2005',
                photo: '../../img/timeline/5.jpg'
            },
            {
                id: 6,
                img: "../../img/timeline/blur/6-blur.jpg",
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Passion',
                sublabel: '2006',
                photo: '../../img/timeline/6.jpg'
            },
            {
                id: 7,
                img: "../../img/timeline/blur/7-blur.jpg",
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'Patience',
                sublabel: '2007',
                photo: '../../img/timeline/7.jpg'
            },
            {
                id: 8,
                img: "../../img/timeline/blur/8-blur.jpg",
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut gravida tortor, lacinia porttitor felis. Etiam vel libero in massa bibendum rhoncus ut in diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                label: 'New Beginning',
                sublabel: '2008',
                photo: '../../img/timeline/8.jpg'
            }
        ];


        /*$scope.timelinePhoto = timelinePhotoItems[0];
        $scope.timelineText = timelineTextItems[0];
        $scope.timelineHeader = timelineHeaderItems[0];
        $("#timeline-background").css("background-image", "url('" + timelineBackgroundItems[0] + "')");
        $(".timeline-photo").css("background-image", "url('" + timelinePhotoItems[0] + "')");
*/
        $scope.page = $scope.pages[0];
        $scope.checked = true;

        $scope.isInTransit = false;

        $scope.svgModNext = function () {
            var nextPage;
            if ($scope.currentPage == nrBreakpoints) {
                nextPage = 0;
            } else {
                nextPage = $scope.currentPage + 1;
            }
            $scope.svgMod(nextPage);
        }

        $scope.svgModPrevious = function () {
            var nextPage;
            if ($scope.currentPage == 0) {
                nextPage = nrBreakpoints;
            } else {
                nextPage = $scope.currentPage - 1;
            }
            $scope.svgMod(nextPage);
        }

        $scope.$on('bgTransitionComplete', function () {
            $scope.isInTransit = false;
        });

        $scope.isCurrentPage = function (page) {
            return $scope.currentPage === page;
        };

        /*        $scope.svgMod = function (page) {
                    var i, k, j;

                    $scope.currentPage = page;

                    if (page != 0) {
                        $scope.checked = false;
                    } else {
                        $scope.checked = true;
                    }

                    $scope.timelineText = timelineTextItems[page];
                    $scope.timelinePhoto = timelinePhotoItems[page];
                    $scope.timelineHeader = timelineHeaderItems[page];

                    $("#timeline-background").css("background-image", "url('" + timelineBackgroundItems[page] + "')");
                    $(".timeline-photo").css("background-image", "url('" + timelinePhotoItems[page] + "')");

                    for (k = 1; k < page + 1; k++) {
                        var firstPoly = $(("#wave" + k));
                        for (j = 0; j < firstPoly.length; j++) {
                            var polygon = firstPoly[j].getElementsByTagName("polygon");
                            for (i = 0; i < polygon.length; i++) {

                                polygon[i].style.fill = "#fff";
                            }
                        }
                    }
                    for (k = page + 1; k < 9; k++) {
                        var firstPoly = $(("#wave" + k));
                        for (j = 0; j < firstPoly.length; j++) {
                            var polygon = firstPoly[j].getElementsByTagName("polygon");
                            for (i = 0; i < polygon.length; i++) {

                                polygon[i].style.fill = 'none';
                            }
                        }
                    }
                }*/

        $scope.svgMod = function (page) {
            var i, k, j;

            /*$scope.currentPage = page;*/

            if (page != 0) {
                $scope.checked = false;
            } else {
                $scope.checked = true;
            }



            /* $("#timeline-background").css("background-image", "url('" + timelineBackgroundItems[page] + "')");
             $(".timeline-photo").css("background-image", "url('" + timelinePhotoItems[page] + "')");*/

            for (k = 1; k < page + 1; k++) {
                var firstPoly = $(("#wave" + k));
                for (j = 0; j < firstPoly.length; j++) {
                    var polygon = firstPoly[j].getElementsByTagName("polygon");
                    for (i = 0; i < polygon.length; i++) {

                        polygon[i].style.fill = "#fff";
                        /*polygon[i].style.transition = "fill 40s ease";*/
                    }
                }
            }
            for (k = page + 1; k < 9; k++) {
                var firstPoly = $(("#wave" + k));
                for (j = 0; j < firstPoly.length; j++) {
                    var polygon = firstPoly[j].getElementsByTagName("polygon");
                    for (i = 0; i < polygon.length; i++) {

                        polygon[i].style.fill = 'none';
                    }
                }
            }

            if ($scope.currentPage !== page) {
                /* $scope.timelineText = timelineTextItems[page];
                 $scope.timelinePhoto = timelinePhotoItems[page];
                 $scope.timelineHeader = timelineHeaderItems[page];*/
                $scope.page = $scope.pages[page];
                $scope.currentPage = page;
                $scope.isInTransit = true;
            }
        }



    });

    mainApp.directive('bg', function ($window) {
        return function (scope, element, attrs) {
            var resizeBG = function () {
                var bgwidth = element.width();
                var bgheight = element.height();

                var winwidth = $window.innerWidth;
                var winheight = $window.innerHeight;

                var widthratio = winwidth / bgwidth;
                var heightratio = winheight / bgheight;

                var widthdiff = heightratio * bgwidth;
                var heightdiff = widthratio * bgheight;

                if (heightdiff > winheight) {
                    element.css({
                        width: winwidth + 'px',
                        height: heightdiff + 'px'
                    });
                } else {
                    element.css({
                        width: widthdiff + 'px',
                        height: winheight + 'px'
                    });
                }
            };

            var windowElement = angular.element($window);
            windowElement.resize(resizeBG);

            element.bind('load', function () {
                resizeBG();
            });
        }
    });

    mainApp.directive('semibg', function ($window) {
        return function (scope, element, attrs) {
            var resizeBG = function () {
                var bgwidth = element.width();
                var bgheight = element.height();

                var winwidth = $window.innerWidth;
                var winheight = $window.innerHeight;

                var widthratio = winwidth / bgwidth;
                var heightratio = winheight / bgheight;

                var widthdiff = heightratio * bgwidth;
                var heightdiff = widthratio * bgheight;

                if (heightdiff > winheight) {
                    winheight = winheight - 250;
                    element.css({
                        width: winwidth + 'px',
                        height: winheight + 'px'
                    });
                } else {
                    winheight = winheight - 250;
                    element.css({
                        width: winwidth + 'px',
                        height: winheight + 'px'
                    });
                }
            };

            var windowElement = angular.element($window);
            windowElement.resize(resizeBG);

            element.bind('load', function () {
                resizeBG();
            });
        }
    });

    mainApp.animation('.bg-animation', function ($window, $rootScope, TweenMax) {
        return {
            enter: function (element, done) {
                TweenMax.fromTo(element, 0.5, {
                    top: $window.innerHeight
                }, {
                    top: 0,
                    onComplete: function () {
                        $rootScope.$apply(function () {
                            $rootScope.$broadcast('bgTransitionComplete');
                        });
                        done();
                    }
                });
            },

            leave: function (element, done) {
                TweenMax.to(element, 0.5, {
                    top: -$window.innerHeight,
                    onComplete: done
                });
            }
        };
    });

    mainApp.animation('.panel-animation-left', function (TweenMax) {
        return {
            addClass: function (element, className, done) {
                if (className == 'ng-hide') {
                    TweenMax.to(element, 0.2, {
                        opacity: 0,
                        onComplete: done
                    });
                } else {
                    done();
                }
            },
            removeClass: function (element, className, done) {
                if (className == 'ng-hide') {
                    element.removeClass('ng-hide');
                    TweenMax.fromTo(element, 0.5, {
                        opacity: 0,
                        left: -element.width()
                    }, {
                        opacity: 0.8,
                        left: 0,
                        onComplete: done
                    });
                } else {
                    done();
                }
            }
        };
    });

    mainApp.animation('.panel-animation-right', function ($window, TweenMax) {
        return {
            addClass: function (element, className, done) {
                if (className == 'ng-hide') {
                    TweenMax.to(element, 0.2, {
                        opacity: 0,
                        onComplete: done
                    });
                } else {
                    done();
                }
            },
            removeClass: function (element, className, done) {
                if (className == 'ng-hide') {
                    element.removeClass('ng-hide');
                    TweenMax.fromTo(element, 0.5, {
                        opacity: 0,
                        top: -element.height()
                    }, {
                        opacity: 0.8,
                        top: ($window.innerHeight / 2),
                        onComplete: done
                    });
                } else {
                    done();
                }
            }
        };
    });

}());