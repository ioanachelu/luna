jQuery(document).ready(function ($) {


    $('.lang').on('click', function () {
        $('.lang_en').css("opacity", "1");
        $('.lang_ro').css("opacity", "1");
        $('.lang').addClass('lang_fixed');
        $('.lang_fixed').removeClass('lang');
        $('.lang_fixed').html('<img src="../img/lang/lateral-nav-left-2.svg" />');

    });

    $('.lang_en').on('click', function () {
        $('.lang_en').css("opacity", "0");
        $('.lang_ro').css("opacity", "0");
        $('.lang_fixed').addClass('lang');
        $('.lang').removeClass('lang_fixed');
        $('.lang').html('<img src="../img/lang/lateral-nav-left.svg" />');

    });

    $('.lang_ro').on('click', function () {
        $('.lang_en').css("opacity", "0");
        $('.lang_ro').css("opacity", "0");
        $('.lang_fixed').addClass('lang');
        $('.lang').removeClass('lang_fixed');
        $('.lang').html('<img src="../img/lang/lateral-nav-left.svg" />');

    });

});