/**
 * modalEffects.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
var ModalEffects = (function() {

	function init() {

		var overlay = document.querySelector( '.md-overlay' );

		[].slice.call( document.querySelectorAll( '.md-trigger' ) ).forEach( function( el, i ) {

			var modal = document.querySelector( '#' + el.getAttribute( 'data-modal' ) ),
				close = modal.querySelector( '.md-close' );

			function removeModal( hasPerspective ) {
				classie.remove( modal, 'md-show' );

				if( hasPerspective ) {
					classie.remove( document.documentElement, 'md-perspective' );
				}
			}

			function removeModalHandler() {
				removeModal( classie.has( el, 'md-setperspective' ) ); 
			}

			el.addEventListener( 'click', function( ev ) {
				classie.add( modal, 'md-show' );
				$("*").find('section').not(".md-modal").not(".md-effect-11").not(".md-show").not(".md-content").css({"filter":"blur(5px)","-webkit-filter": "blur(5px)","-moz-filter": "blur(5px)",  "-o-filter": "blur(5px)",  "-ms-filter": "blur(5px)"});
				disable_scroll();
				if( classie.has( el, 'md-setperspective' ) ) {
					setTimeout( function() {
						classie.add( document.documentElement, 'md-perspective' );
					}, 25 );
				}
			});

			close.addEventListener( 'click', function( ev ) {
				ev.stopPropagation();
				removeModalHandler();
				enable_scroll();
				$("*").find('section').not(".md-modal").not(".md-effect-11").not(".md-show").not(".md-content").css({"filter":"blur(0px)","-webkit-filter": "blur(0px)","-moz-filter": "blur(0px)",  "-o-filter": "blur(0px)",  "-ms-filter": "blur(0px)"});
				
			});

		} );

	}
        // left: 37, up: 38, right: 39, down: 40,
        // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
        var keys = [37, 38, 39, 40];

        function preventDefault(e) {
          e = e || window.event;
          if (e.preventDefault)
              e.preventDefault();
          e.returnValue = false;  
        }

        function keydown(e) {
            for (var i = keys.length; i--;) {
                if (e.keyCode === keys[i]) {
                    preventDefault(e);
                    return;
                }
            }
        }

        function wheel(e) {
          preventDefault(e);
        }

        function disable_scroll() {

          if (window.addEventListener) {
              window.addEventListener('DOMMouseScroll', wheel, false);
          }
          window.onmousewheel = document.onmousewheel = wheel;
          document.onkeydown = keydown;
        }

        function enable_scroll() {
            if (window.removeEventListener) {
                window.removeEventListener('DOMMouseScroll', wheel, false);
            }
            window.onmousewheel = document.onmousewheel = document.onkeydown = null;  
        }
            init();

})();